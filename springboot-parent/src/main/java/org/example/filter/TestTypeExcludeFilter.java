package org.example.filter;

import org.example.controller.HelloController;
import org.springframework.boot.context.TypeExcludeFilter;
import org.springframework.core.type.classreading.MetadataReader;
import org.springframework.core.type.classreading.MetadataReaderFactory;

import java.io.IOException;

public class TestTypeExcludeFilter extends TypeExcludeFilter {

    @Override
    public boolean match(MetadataReader metadataReader, MetadataReaderFactory metadataReaderFactory) throws IOException {
//        if(metadataReader.getClassMetadata().getClass()== HelloController.class){
//            return false;
//        }
        return super.match(metadataReader, metadataReaderFactory);
    }
}
