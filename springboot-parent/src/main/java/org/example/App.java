package org.example;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Collections;

/**
 * @SpringBootApplication 来标注一个主程序类，说明这是一个Spring Boot应用
 * 自动装配就是从这里开始的
 */
@SpringBootApplication
public class App 
{
    public static void main( String[] args )
    {

        SpringApplication springApplication = new SpringApplication();
//        springApplication.setBannerMode(Banner.Mode.OFF);
        springApplication.run(App.class,args);

        System.out.println(System.getProperty("CONSOLE_LOG_PATTERN"));
//        SpringApplication.run(App.class,args);
        Logger logger = LoggerFactory.getLogger(App.class);
        logger.info(logger.getClass().toString());
        logger.error("这是一条测试短信！");

    }
}
