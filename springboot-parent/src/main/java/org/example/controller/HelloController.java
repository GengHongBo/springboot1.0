package org.example.controller;

import org.example.beans.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/hello")
public class HelloController {

    @Value("${person.name}")
    private String name;

    @RequestMapping("/world")
    @ResponseBody
    public String hello(){
        System.out.println("hello world!");
        return "hello world!"+name;
    }

    @Autowired
    private Person person;

    @RequestMapping("/person")
    @ResponseBody
    public String person(){
        System.out.println("hello person!");
        return "hello world!"+person;
    }

}
