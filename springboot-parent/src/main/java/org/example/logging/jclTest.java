package org.example.logging;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.logging.Logger;

public class jclTest {
    public static void main(String[] args) {
//        Logger logger = Logger.getLogger(jclTest.class.getName());
//        logger.info("官方日志：jul");

        Log log = LogFactory.getLog(jclTest.class);
        log.info(log.getClass());
        log.info("官方日志：jcl+jul");

        log.info("CONSOLE_LOG_PATTERN:"+System.getProperty("CONSOLE_LOG_PATTERN"));
    }
}
