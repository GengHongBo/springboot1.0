package com.example.freemarker.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping("controller")
public class userController {

    @RequestMapping("/index")
    public String getIndex(Model model){
        model.addAttribute("username","world!");
        return "index";
    }

    @RequestMapping("/list")
    public String getList(Model model){
        List<String> testList = Arrays.asList("test", "test1", "test2");
        model.addAttribute("testList",testList);
        return "list";
    }
}
