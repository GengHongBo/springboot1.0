package com.example.servlet.config;

import com.example.servlet.customer.HelloServlet;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.Servlet;

@Configuration
public class MyMvcConfigure {

    @Bean
    public ServletRegistrationBean getBean(){
        ServletRegistrationBean servletServletRegistrationBean = new ServletRegistrationBean();
        servletServletRegistrationBean.setServlet(new BeanServlet());//设置servlet
        servletServletRegistrationBean.addUrlMappings("/BeanServlet");//设置映射规则
        servletServletRegistrationBean.setName("BeanServlet");//设置名字
        return servletServletRegistrationBean;
    }
}
