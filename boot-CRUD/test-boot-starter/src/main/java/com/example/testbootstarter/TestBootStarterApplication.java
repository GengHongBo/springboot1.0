package com.example.testbootstarter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestBootStarterApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestBootStarterApplication.class, args);
    }

}
