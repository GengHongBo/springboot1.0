package com.example.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ResourceBundleMessageSource;

import javax.servlet.http.HttpServletRequest;

@Configuration
public class I18nMessageUtil {

    @Autowired
    private static ResourceBundleMessageSource messageSource;

    @Autowired
    private static HttpServletRequest request;

    /**
     * 获取国际化资源属性
     * @param code
     * @param args
     * @return
     */
    public static String getMessage(String code,String... args){
        return messageSource.getMessage(code, args, request.getLocale());
    }

//    public void setMessageSource(ResourceBundleMessageSource messageSource) {
//        this.messageSource = messageSource;
//    }
//    @Autowired
//    public void setRequest(HttpServletRequest request) {
//        this.request = request;
//    }

}