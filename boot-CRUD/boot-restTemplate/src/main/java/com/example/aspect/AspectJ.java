package com.example.aspect;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.logging.LogFactory;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect//标识为一个切面
@Component
public class AspectJ {

    private Logger logger = LoggerFactory.getLogger(AspectJ.class);

    //环绕通知
    @Around("execution(* com.example.controller.*.*(..)) &&@annotation(operation)")
    public Object around(ProceedingJoinPoint joinPoint, ApiOperation operation) throws Throwable {

        //查找ApiOperation并打上日志信息
        //1.查找类信息
        Class<?> aClass = joinPoint.getThis().getClass();
//        2.找到Api注解信息
        Api api = aClass.getAnnotation(Api.class);
//        3.不为空的情况下得到打印日志
        if(null != api){
            logger.info("api注解："+api.value());
        }

//        4.打印ApiOperation注解信息
        String value = operation.value();
        logger.info("ApiOperation注解信息："+value);
        //继续执行
        return joinPoint.proceed();
    }
}
