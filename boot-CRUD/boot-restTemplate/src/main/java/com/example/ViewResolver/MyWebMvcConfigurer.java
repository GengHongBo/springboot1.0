package com.example.ViewResolver;

import com.example.interCeptor.TimeInterCeptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.*;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;

/**
 * 定制springMVC
 */
//@EnableWebMvc
@Configuration
public class MyWebMvcConfigurer implements WebMvcConfigurer {

    //添加拦截器
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new TimeInterCeptor())//添加拦截器
        .addPathPatterns("/**")//添加拦截规则
        .excludePathPatterns("/static/*")//添加排除规则
        ;

        //添加国际化拦截器
        registry.addInterceptor(new LocaleChangeInterceptor())
                .addPathPatterns("/**");//添加拦截规则
    }

    //添加本地国际化缓存
    @Bean
    public LocaleResolver localeResolver(){
        CookieLocaleResolver cookieLocaleResolver = new CookieLocaleResolver();
        //设置过期时间
        cookieLocaleResolver.setCookieMaxAge(60*60*24*30);
        cookieLocaleResolver.setCookieName("locale");
        return cookieLocaleResolver;
    }

    //添加视图解析器
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
//        registry.addViewController("/helloTest")//添加URL后缀相当于
//                .setViewName("hello");

        registry.addViewController("/helloTest")//添加URL后缀相当于
                .setViewName("hello.html");
    }

    //添加全局CORS配置

//    @Override
//    public void addCorsMappings(CorsRegistry registry) {
//        registry.addMapping("/userController/*")//设置本服务器中跨域访问的接口映射
//        .allowedOrigins("http://localhost:8081")//设置其他服务器可以跨域访问的URL
//        .allowedMethods("GET","POST","DELETE","PUT");//设置跨域访问的方式
//    }


}

