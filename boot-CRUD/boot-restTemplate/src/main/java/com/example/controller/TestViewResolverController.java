package com.example.controller;

import com.example.beans.Result;
import com.example.beans.User;
import com.example.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * GetMapping
 * PostMapping
 *
 */
@Controller
@RequestMapping("/testViewResolverController")
public class TestViewResolverController {

    @RequestMapping("/getViewResolver")
    public String getViewResolver(){
        return "testBeanNameResolver";
    }
}
