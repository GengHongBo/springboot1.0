package com.example.controller;

import com.example.beans.Result;
import com.example.beans.User;
import com.example.config.I18nMessageUtil;
import com.example.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.bind.annotation.*;

/**
 * GetMapping
 * PostMapping
 *
 */
@RestController
@RequestMapping("/userController")
@Api("描述类信息")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private MessageSource messageSource;

    @GetMapping("/{id}")
    @ApiOperation("描述方法信息")
    @ApiImplicitParam(name="id",value = "参数ID",defaultValue = "1",required = true)
    @ApiResponse(code = 200,message = "返回信息",response = Result.class)
    @CrossOrigin("http://localhost:8081")
    public Result getUser(@PathVariable Integer id){
        //获取资源文件code值
        String message = messageSource.getMessage("user.query.success", null, LocaleContextHolder.getLocale());
//        String message = I18nMessageUtil.getMessage("user.query.success", "locale");
        System.out.println("message:"+message);
        User user = userService.getUserById(id);
        return new Result<>(200,message,user);
    }

    /**
     * @RequestBody接收jaskson数据
     * @param user
     * @return
     */
    @PostMapping("/add")
    public Result addUser(@RequestBody User user){
        System.out.println(user);
        userService.add(user);
        return new Result(200,"插入成功",userService.getAllUser());
    }

    @PutMapping("/editUser")
    public Result editUser(@RequestBody User user){
        userService.update(user);
        return new Result(200,"插入成功",userService.getAllUser());
    }

    @DeleteMapping("/{id}")
    public Result deleteUser(@PathVariable Integer id){
        userService.delete(id);
        return new Result(200,"插入成功",userService.getAllUser());
    }
}
