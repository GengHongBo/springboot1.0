package com.example;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MockMvcBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest
@AutoConfigureMockMvc //专门用于mocktest，依赖于spring-boot-test下的junit依赖库
public class MockMvcTest {

//    @Test
//    void testWithMockMvc(@Autowired MockMvc mvc) throws Exception {
//
//        mvc.perform(
//                MockMvcRequestBuilders.get("/userController/{id}",1)//发起请求
//                .accept(MediaType.APPLICATION_JSON_UTF8)//设置为utf-8格式
////                .param() //param方法绑定？后面的参数
//        ).andExpect(MockMvcResultMatchers.status().isOk()) //代表状态码返回200
//                //测试body里面的数据是否是想要的数据
//                .andExpect(MockMvcResultMatchers.jsonPath("$.data.username").value("zhang3"))
//        .andDo(MockMvcResultHandlers.print());//打印内容
//    }
//
//    @Test
//    void testWithMockMvcPost(@Autowired MockMvc mvc) throws Exception {
//
//        String json = "{\n" +
//                "  \"username\": \"zhangsan\",\n" +
//                "  \"address\": \"test\"\n" +
//                "}";
//
//        mvc.perform(
//                MockMvcRequestBuilders.post("/userController/add")//发起请求
//                        .accept(MediaType.APPLICATION_JSON_UTF8)//设置响应的文本类型为utf-8
//                .contentType(MediaType.APPLICATION_JSON_UTF8) //设置请求的文本类型
//                .content(json)
//        ).andExpect(MockMvcResultMatchers.status().isOk()) //代表状态码返回200
//                .andDo(MockMvcResultHandlers.print());//打印内容
//    }
}
