package org.example;

import junit.framework.TestCase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * Unit test for simple App.
 */
@SpringBootTest(classes = App.class) //springboot test支持
@WebAppConfiguration //按照web形式测试
@RunWith(SpringJUnit4ClassRunner.class)
public class AppTest 
{

    @Autowired
    private App app;
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        TestCase.assertEquals(app.home(),"Hello World!");
    }
}
